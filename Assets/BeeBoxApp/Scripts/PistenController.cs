﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistenController : MonoBehaviour
{
    public Transform joint;
    #region Field and Property
    /// <summary>
    /// Keep up mode.
    /// </summary>
    /// 
    public enum KeepUpMode
    {
        TransformUp,
        ReferenceForward
    }
    public KeepUpMode keepUp = KeepUpMode.TransformUp;

    /// <summary>
    /// Reference forward as world up for look at.
    /// </summary>
    public Transform reference;

    /// <summary>
    /// World up for look at.
    /// </summary>
    public Vector3 WorldUp
    {
        get
        {
            var up = transform.up;
            if (keepUp == KeepUpMode.ReferenceForward && reference)
            {
                up = reference.forward;
            }
            return up;
        }
    }
    #endregion

    public Transform hookObject;

    public bool isHookable=false;

    public void Update()
    {
        Debug.Log(Vector3.Distance(transform.position,joint.position));
        transform.LookAt(joint, WorldUp);
        if (isHookable)
        {
            transform.position = hookObject.position;
        }
    }

}

