﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BeeboxCat
{
    public class AccessoriesBtn : MonoBehaviour, IPointerClickHandler
    {
        public AccessoriesProperty accessoriesProperty;
        private int accessoriesID;
        private Image buttonIcon;
        public bool isActive;
        void Awake()
        {
            InitButton();
        }

        public void InitButton()
        {
                buttonIcon = transform.GetChild(0).GetComponent<Image>();
            if (accessoriesProperty != null)
            {
                accessoriesID = (int)accessoriesProperty.ID;
                buttonIcon.sprite = accessoriesProperty.buttonIcon;
                OnActiveButton();
            }
            else
            {
                OnInActiveButton();
            }
        }

        public void OnActiveButton()
        {
            isActive = true;
           // GetComponent<Image>().color = new Color(1, 1, 1);
            buttonIcon.color = new Color(1, 1, 1);
        }

        public void OnInActiveButton()
        {
            isActive = false;
           // GetComponent<Image>().color = new Color32((byte)0, (byte)0, (byte)0, (byte)130);
            buttonIcon.color = new Color32((byte)0, (byte)0, (byte)0, (byte)70);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (isActive)
            {
                AppManager.instance.accessoriesCustomization.OnAttachmentChange(accessoriesID);
            }
        }
    }
}
