﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


namespace BeeboxCat
{
    public class ServerDataManager : MonoBehaviour
    {

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR

        string mailServerPath = "http://www.beebox3d.com/cat216b3/contactmail.php";
        string addDataBaseServerPath = "http://www.beebox3d.com/cat216b3/addinapptime.php";
        string updateDataBaseServerPath = "http://www.beebox3d.com/cat216b3/updateinapptime.php";
#endif
#if UNITY_EDITOR
        string mailServerPath = "http://localhost:7777/arcat/contactmail.php";
        string addDataBaseServerPath = "http://localhost:7777/arcat/addinapptime.php";
        string updateDataBaseServerPath = "http://localhost:7777/arcat/updateinapptime.php";
#endif

        int index_number;

        public IEnumerator SetControlState(string sub, string msg)
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();

            formData.Add(new MultipartFormDataSection("subject", sub.ToString()));
            formData.Add(new MultipartFormDataSection("message", msg.ToString()));
            UnityWebRequest curRequest = UnityWebRequest.Post(mailServerPath, formData);
            yield return curRequest.SendWebRequest();
            yield return null;
        }


        public IEnumerator AddDBData(string _inAppStartTime, string _inAppEndTime, string _timeZone, string _totalTime, int _ObjPlaceCount, string gpsdata)
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("deviceuid", SystemInfo.deviceUniqueIdentifier.ToString()));
            formData.Add(new MultipartFormDataSection("intime", _inAppStartTime.ToString()));
            formData.Add(new MultipartFormDataSection("outtime", _inAppEndTime.ToString()));
            formData.Add(new MultipartFormDataSection("timezone", _timeZone.ToString()));
            formData.Add(new MultipartFormDataSection("totaltime", _totalTime.ToString()));
            formData.Add(new MultipartFormDataSection("arplacecount", _ObjPlaceCount.ToString()));
            formData.Add(new MultipartFormDataSection("gps", gpsdata.ToString()));
            UnityWebRequest curRequest = UnityWebRequest.Post(addDataBaseServerPath, formData);
            yield return curRequest.SendWebRequest();

            if (curRequest.isNetworkError || curRequest.isHttpError)
            {

            }
            else
            {
                Debug.Log(curRequest.downloadHandler.text);
                index_number = int.Parse(curRequest.downloadHandler.text);
            }
            yield return null;
        }

        public IEnumerator UpdateTimeData(string _inAppEndTime, string _totalTimeToDisplay, int _ObjPlaceCount)
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("indexid", index_number.ToString()));
            formData.Add(new MultipartFormDataSection("outtime", _inAppEndTime.ToString()));
            formData.Add(new MultipartFormDataSection("totaltime", _totalTimeToDisplay.ToString()));
            formData.Add(new MultipartFormDataSection("arplacecount", _ObjPlaceCount.ToString()));
            UnityWebRequest curRequest = UnityWebRequest.Post(updateDataBaseServerPath, formData);
            yield return curRequest.SendWebRequest();
            yield return null;
        }




        public bool isInternetConnected()
        {
            bool status = false;
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }
    }
}
