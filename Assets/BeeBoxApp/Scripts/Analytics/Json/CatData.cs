﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace BeeboxCat
{

    [System.Serializable]
    public class CatData
    {
        public List<MailData> MailDataList;
        public List<DBData> DBDataList;
        MailData mailData;
        DBData dbData;
        public CatData() //Retrieval of data for demo
        {

        }
        public void SaveMailDataLocally(string custName, string emailID, string contactNo, string custArea, string gpsData, string loginTime, string deviceId, string curBuildVersionName, string message)
        {
            mailData = new MailData();
            mailData.contactNo = contactNo;
            mailData.curBuildVersionName = curBuildVersionName;
            mailData.custArea = custArea;
            mailData.custName = custName;
            mailData.deviceId = deviceId;
            mailData.emailID = emailID;
            mailData.gpsData = gpsData;
            mailData.loginTime = loginTime;
            mailData.message = message;
            DataMgr.instance.localDataMgr.AddMailData(mailData);
        }

        public void SaveDBDataLocally(string deviceid, string inAppStartTime, string inAppEndTime, string timeZone, string totalTime, int ObjPlaceCount, string gps)
        {
            dbData = new DBData();
            dbData.deviceuid = deviceid;
            dbData.inAppStartTime = inAppStartTime;
            dbData.inAppEndTime = inAppEndTime;
            dbData.timeZone = timeZone;
            dbData.totalTime = totalTime;
            dbData.ObjPlaceCount = ObjPlaceCount;
            dbData.gps = gps;
            DataMgr.instance.localDataMgr.AddDBData(dbData);
        }


    }
}

