﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;
using System.Text;
using System.Linq;


namespace BeeboxCat
{
    public class JsonReadWrite : MonoBehaviour
    {
        public string GetFilePath()
        {
            return Application.persistentDataPath + "/" + "Beebox.json";
        }

        public void AddToJson(CatData catData)
        {
            string saveDataString = JsonUtility.ToJson(catData);
            JSONFileUtility.WriteJsonToExternalResource("Beebox.json", saveDataString);
            //  Debug.Log(Application.persistentDataPath + "/" + "Beebox.json");
        }

        public void ClearJson()
        {
            string clearData = "";
            string saveDataString = JsonUtility.ToJson(clearData);
            JSONFileUtility.WriteJsonToExternalResource("Beebox.json", saveDataString);
        }

        public CatData GetJsonData()
        {
            string filePath = Application.persistentDataPath + "/" + "Beebox.json";
            CatData catData = null;
            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                catData = JsonUtility.FromJson<CatData>(dataAsJson);
            }
            return catData;
        }


        public class JSONFileUtility
        {
            public static void WriteJsonToExternalResource(string path, string content)
            {
                path = Application.persistentDataPath + "/" + path;
                // Debug.Log(path);
                FileStream stream = File.Create(path);
                byte[] contentBytes = new UTF8Encoding(true).GetBytes(content);
                stream.Write(contentBytes, 0, contentBytes.Length);
                stream.Close();
            }
        }

    }
}
