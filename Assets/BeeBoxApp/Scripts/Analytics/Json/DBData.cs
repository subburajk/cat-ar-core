﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DBData
{
    public string deviceuid;
    public string inAppStartTime;
    public string inAppEndTime;
    public string timeZone;
    public string totalTime;
    public int ObjPlaceCount;
    public string gps;
}

