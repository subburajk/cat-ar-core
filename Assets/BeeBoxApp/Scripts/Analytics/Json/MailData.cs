﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MailData
{
    public int itemId;
    public string custName;
    public string emailID;
    public string contactNo;
    public string custArea;
    public string gpsData;
    public string loginTime;
    public string deviceId;
    public string curBuildVersionName;
    public string message;
}

