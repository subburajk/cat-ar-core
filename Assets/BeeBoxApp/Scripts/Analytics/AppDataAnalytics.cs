﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace BeeboxCat
{
    public class AppDataAnalytics : MonoBehaviour
    {
        const string ISLOGIN = "loginStaus";
        string username;
        string password;
        public InputField usernameText;
        public InputField passwordText;
        bool isCredentialsCorrect = false;
        public GameObject ContactForm;
        public GameObject LoginForm;
        bool isAlreadyLogin = false;
        public Text wrongPassword;

        string custName;
        string emailID;
        string contactNo;
        string custArea;
        string subject;
        string message;
        string Data;
        string gpsData;
        string loginTime;
        string deviceId;
        public InputField custNameText;
        public InputField emailIDTexxt;
        public InputField contactNoText;
        public InputField custAreaText;
        public InputField messageText;
        bool isMsgSend = false;
        string curBuildVersionName;

        string inAppStartTime = "0";
        string inAppEndTime = "0";
        float totalTime = 0;
        string timeZone;
        bool isPaused = false;
        int timeInSec;
        int timeInMn;
        string totalTimeToDisplay = "0";
        public int ObjPlaceCount = 0;
        CatData catData;
        bool isDBAdded;
        // Start is called before the first frame update
        void Start()
        {
            totalTime = 0;
            isDBAdded = false;
               catData = DataMgr.instance.localDataMgr.GetCatData();
            if (PlayerPrefs.GetString(ISLOGIN, "0").Equals("1"))
            {
                isAlreadyLogin = true;
            }
            username = "CAT216B3";
            password = "2o1gAR_$0L";  //2o1gAR_$0L
            if (isAlreadyLogin)
            {
                ContactForm.SetActive(true);
                LoginForm.SetActive(false);
            }
            else
            {
                ContactForm.SetActive(false);
                LoginForm.SetActive(true);
            }
            custName = "Customer Name :";
            emailID = "EmailID : ";
            contactNo = "Contact No : ";
            custArea = "Customer Area : ";
            subject = "CAT216B3 Demo";
            message = "Customer Application : ";
            loginTime = "Login Time : ";
            deviceId = "Device ID : ";
            gpsData = "GPS : ";
            curBuildVersionName = "CurrentBuildVersionName : CAT216B3_IOS_V2 ";


#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
            {
                Permission.RequestUserPermission(Permission.FineLocation);
            }
#endif
            StartCoroutine(CheckGpsData());

            inAppStartTime = DateTime.Now.ToString();
            timeZone = TimeZoneInfo.Local.ToString();
            catData = DataMgr.instance.localDataMgr.GetCatData();
           
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown("space"))
            {
                PlayerPrefs.SetString(ISLOGIN, "0");
            }
            if (!isPaused)
            {
                //Debug.Log(totalTime);
                totalTime += Time.deltaTime;
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                GetCurrentDataUpdate();
                Debug.Log("updated" + inAppEndTime);
                StartCoroutine(DataMgr.instance.serverDataMgr.UpdateTimeData(inAppEndTime, totalTimeToDisplay, ObjPlaceCount));
            }
        }
        bool checkCredentials()
        {
            if (usernameText.text.Equals(username) && passwordText.text.Equals(password))
            {
                isCredentialsCorrect = true;
            }
            else
            {
                isCredentialsCorrect = false;
            }
            return isCredentialsCorrect;
        }

        public void OnInputFiledValueChange()
        {
            wrongPassword.text = " ";
        }

        public void OnLoginOkBtnClicked()
        {
            if (checkCredentials())
            {
                PlayerPrefs.SetString(ISLOGIN, "1");
                ContactForm.SetActive(true);
                LoginForm.SetActive(false);
                wrongPassword.text = " ";
            }
            else
            {
                usernameText.text = " ";
                passwordText.text = " ";
                wrongPassword.text = "Incorrect Credentials";
            }
        }


        public void GetInputData()
        {
            custName += custNameText.text;
            emailID += emailIDTexxt.text;
            contactNo += contactNoText.text;
            custArea += custAreaText.text;
            loginTime += System.DateTime.Now;
            deviceId += SystemInfo.deviceUniqueIdentifier;
            message += messageText.text;

            Data = custName + "\n " + emailID + "\n " + contactNo + "\n" + custArea + "\n" + gpsData + "\n" + loginTime +
                "\n" + deviceId + "\n" + curBuildVersionName + "\n" + message;


        }


        public void OnSubmitBtnClicked()
        {
            GetInputData();
            GetCurrentDataUpdate();
             isDBAdded = true;
            if (DataMgr.instance.serverDataMgr.isInternetConnected())
            {
                //checkPrevData
                StartCoroutine(DataMgr.instance.localDataMgr.SendAlreadySavedDBData());
                StartCoroutine(DataMgr.instance.serverDataMgr.SetControlState(subject, Data));
                StartCoroutine(DataMgr.instance.serverDataMgr.AddDBData(inAppStartTime, inAppEndTime, timeZone, totalTimeToDisplay, ObjPlaceCount, gpsData));
            }
            else
            {
                SaveDBDataLocally();
                SaveMailDataLocally();
                //add existind data local
                Debug.Log("Error. Check internet connection!");
            }
            DataMgr.instance.sceneMgr.AcitivateScene();
        }


        IEnumerator CheckGpsData()
        {
            // First, check if user has location service enabled
            if (!Input.location.isEnabledByUser)
                yield break;

            // Start service before querying location
            Input.location.Start();

            // Wait until service initializes
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            // Service didn't initialize in 20 seconds
            if (maxWait < 1)
            {
                print("Timed out");
                yield break;
            }

            // Connection has failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                print("Unable to determine device location");
                yield break;
            }
            else
            {
                // Access granted and location value could be retrieved
                print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude +
                    " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
                gpsData = "Location:" + Input.location.lastData.latitude + "," + Input.location.lastData.longitude + "\n" +
                " latitude : " + Input.location.lastData.latitude + "\n longitude " + Input.location.lastData.longitude + " \n altitude " + Input.location.lastData.altitude +
                    " \n horizontalAccuracy " + Input.location.lastData.horizontalAccuracy + " \n timestamp " + Input.location.lastData.timestamp;
            }

            // Stop service if there is no need to query location updates continuously
            Input.location.Stop();
        }


        public void SaveMailDataLocally()
        {
            catData.SaveMailDataLocally(custName, emailID, contactNo, custArea, gpsData, loginTime, deviceId, curBuildVersionName, message);
        }

        public void SaveDBDataLocally()
        {
            catData.SaveDBDataLocally(SystemInfo.deviceUniqueIdentifier, inAppStartTime, inAppEndTime,
                    timeZone.ToString(), totalTimeToDisplay, ObjPlaceCount, gpsData);
        }

        public void UpdateDBData()
        {
            if (catData != null)
            {
                for (int i = 0; i < catData.DBDataList.Count; i++)
                {
                    if (catData.DBDataList[i].inAppStartTime == inAppStartTime)
                    {
                        catData.DBDataList[i].inAppEndTime = inAppEndTime;
                        catData.DBDataList[i].totalTime = totalTimeToDisplay;
                        catData.DBDataList[i].ObjPlaceCount = ObjPlaceCount;
                        DataMgr.instance.jsonReadWrite.AddToJson(catData);
                    }
                }
            }

        }

        public void GetCurrentDataUpdate()
        {
            inAppEndTime = DateTime.Now.ToString();
            Debug.Log(totalTime);
            timeInMn = (int)totalTime / 60;
            timeInSec = (int)totalTime % 60;
            totalTimeToDisplay = timeInMn.ToString("0.##") + " : " + timeInSec.ToString("0.##");
            Debug.Log(totalTimeToDisplay);

        }


        public void OnApplicationPause(bool pause)
        {
            GetCurrentDataUpdate();
            Debug.Log(inAppEndTime + "" + ObjPlaceCount);
            if (isDBAdded)
            {
                if (DataMgr.instance.serverDataMgr.isInternetConnected())
                {
                    StartCoroutine(DataMgr.instance.serverDataMgr.UpdateTimeData(inAppEndTime, totalTimeToDisplay, ObjPlaceCount));
                }
                else
                {
                    UpdateDBData();

                }
            }
            if (pause)
            {
                isPaused = true;
            }
            else
            {
                isPaused = false;
            }
        }

    }
}
