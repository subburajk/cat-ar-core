﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;

namespace BeeboxCat
{
    public class LocalDataMgr : MonoBehaviour
    {
        CatData catData;
        string filePath;

        private void Start()
        {
            filePath = DataMgr.instance.jsonReadWrite.GetFilePath();
            CatData shop = new CatData();
            catData = DataMgr.instance.jsonReadWrite.GetJsonData();
            string writeJsonString = JsonUtility.ToJson(catData);
            if (writeJsonString == "")
            {
                DataMgr.instance.jsonReadWrite.AddToJson(shop);
            }
            CatDataParser.ParseCatDataJson(ref catData, filePath);

            if (DataMgr.instance.serverDataMgr.isInternetConnected())
            {
                StartCoroutine(SendAlreadySavedMailData());
            }

        }


        public void UpdateCatData(CatData shop)
        {
            DataMgr.instance.jsonReadWrite.AddToJson(shop);
        }

        public void AddMailData(MailData mailData)
        {

            catData.MailDataList.Add(mailData);
            DataMgr.instance.jsonReadWrite.AddToJson(catData);
        }

        public void AddDBData(DBData dBData)
        {
            catData.DBDataList.Add(dBData);
            DataMgr.instance.jsonReadWrite.AddToJson(catData);
        }

        public CatData GetCatData()
        {
            return catData;
        }

        public IEnumerator SendAlreadySavedMailData()
        {
            // CatDataParser.ParseCatDataJson(ref catData, filePath);
            int count = catData.MailDataList.Count;
            for (int i = 0; i < catData.MailDataList.Count; i++)
            {
                string Data = catData.MailDataList[i].custName + "\n " + catData.MailDataList[i].emailID + "\n " + catData.MailDataList[i].contactNo + "\n" +
                      catData.MailDataList[i].custArea + "\n" + catData.MailDataList[i].gpsData + "\n" + catData.MailDataList[i].loginTime + "\n" +
                      catData.MailDataList[i].deviceId + "\n" + catData.MailDataList[i].curBuildVersionName + "\n" + catData.MailDataList[i].message;
                yield return StartCoroutine(DataMgr.instance.serverDataMgr.SetControlState("CAT216B3 Demo", Data));
                count--;
            }
            if (count <= 0)
            {
                catData.MailDataList = new List<MailData>();
                DataMgr.instance.localDataMgr.UpdateCatData(catData);
            }

            yield return null;
        }


        public IEnumerator SendAlreadySavedDBData()
        {
            if (catData.DBDataList != null)
            {
                int count = catData.DBDataList.Count;
                for (int i = 0; i < catData.DBDataList.Count; i++)
                {
                    yield return StartCoroutine(DataMgr.instance.serverDataMgr.AddDBData(catData.DBDataList[i].inAppStartTime, catData.DBDataList[i].inAppEndTime, catData.DBDataList[i].timeZone, catData.DBDataList[i].totalTime, catData.DBDataList[i].ObjPlaceCount, catData.DBDataList[i].gps));
                    count--;
                }
                if (count <= 0)
                {
                    catData.DBDataList = new List<DBData>();
                    DataMgr.instance.localDataMgr.UpdateCatData(catData);
                }
            }

            yield return null;
        }
    }
}
