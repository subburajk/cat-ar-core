﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace BeeboxCat
{
    public class AnimationInvokeEvent : MonoBehaviour
    {

        public UnityEvent animationStartEvents;
        public UnityEvent animationEndEvents;

        public void OnStartCallActivity()
        {
            animationStartEvents.Invoke();
        }

        public void OnEndCallActivity()
        {
            animationEndEvents.Invoke();
        }

    }
}
