﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{

    [SerializeField] private Transform target;
    [SerializeField] private float smoothTime = 0.2f;

    private Vector3 smoothV, newAngles;
    void OnDrawGizmos()
    {
        Vector3 direction = (target.position - transform.position);
        direction.x = 0f;
        Gizmos.DrawRay(transform.position, direction);
        //transform.LookAt(direction);
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            //Vector3 direction = (target.position - transform.position);
            //direction.x = 0f;
            //transform.LookAt(direction,Vector3.forward);
            //#region FirstMethod
            //Vector3 direction = (target.position - transform.position).normalized;
            //direction.x = 0f;
            //float angle = transform.eulerAngles.y + Vector3.SignedAngle(-transform.forward, direction, transform.forward);
            //newAngles = new Vector3(transform.eulerAngles.x, angle, transform.eulerAngles.z);
            //#endregion

            #region SecondMethod
             newAngles = transform.LookAtOneAxis(target.position, -transform.forward, transform.up, PistenRotationExtension.RotationAxis.Up, PistenRotationExtension.RotationSpace.Local);
            #endregion
             transform.eulerAngles = Vector3.SmoothDamp(transform.eulerAngles, newAngles, ref smoothV, smoothTime);


        }
    }
}