﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class Scaler : MonoBehaviour
    {
        public Transform target;
        // Start is called before the first frame update
        public float scalefactor;

        public Vector3 startingPoint;


        public void Awake()
        {
            startingPoint = transform.position;
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, ((target.localPosition.z - startingPoint.z) * scalefactor) / 2);
        }

        // Update is called once per frame
        void Update()
        {

            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, ((target.localPosition.z - startingPoint.z) * scalefactor) / 2);

        }
    }
}
