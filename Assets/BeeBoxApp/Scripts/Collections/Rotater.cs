﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class Rotater : MonoBehaviour
    {

        public Transform target;

        private bool startScale = false;

        [SerializeField]
        private Transform parentObj;

        public bool isUpdateParentObj = false;
        private bool isKeepUpdate = false;

        private void Start()
        {
            StartCoroutine(startScaling());
        }

        // Update is called once per frame
        void Update()
        {
            transform.LookAt(target);

            if (isKeepUpdate)
            {
                transform.position = parentObj.position;
            }
        }

        IEnumerator startScaling()
        {
            transform.position = parentObj.position;
            if (isUpdateParentObj)
            {
                isKeepUpdate = true;
            }
            yield return null;
        }
    }
}
