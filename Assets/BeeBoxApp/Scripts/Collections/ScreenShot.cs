﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class ScreenShot : MonoBehaviour
    {
        private int SC;
      
        public void TakeScreenShot()
        {
            StartCoroutine(TakeScreenshotAndSave());
        }

        private IEnumerator TakeScreenshotAndSave()
        {
            AppManager.instance.uIMGR.HideAllUI();

            yield return new WaitForEndOfFrame();


            Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            ss.Apply();

            yield return new WaitForEndOfFrame();

            AppManager.instance.uIMGR.ShowAllUI();

            // Save the screenshot to Gallery/Photos
            //Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(ss, "GalleryTest", "Cat {0}.png"));
            NativeGallery.SaveImageToGallery(ss, "Cat-216B3", "com.beebox.Cat216B3-{0}.jpg");

            // To avoid memory leaks
            Destroy(ss);
        }
    }
}
