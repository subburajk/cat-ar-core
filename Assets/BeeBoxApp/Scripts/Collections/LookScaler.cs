﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class LookScaler : MonoBehaviour
    {
        public Transform target;
        public float extendVal;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            float dist = Vector3.Distance(this.transform.position, target.position);
            this.transform.localScale = new Vector3(this.transform.localScale.x, dist, this.transform.localScale.z);
        }
    }
}
