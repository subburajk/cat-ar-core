﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class LookRotater : MonoBehaviour
    {

        public Transform target;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 relativePos = target.position - transform.position;
            Quaternion rot = Quaternion.LookRotation(relativePos, Vector3.forward);
            rot.x = 0;
            rot.y = 0;
            transform.localRotation = rot;
        }
    }
}
