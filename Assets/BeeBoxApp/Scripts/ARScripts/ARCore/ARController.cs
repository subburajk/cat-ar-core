﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
namespace BeeboxCat
{
    public class ARController : MonoBehaviour
    {

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        /// </summary>\\
        /// 

        public Camera FirstPersonCamera;

        public GameObject DetectedPlanePrefab;

        private bool m_IsQuitting = false;

        GameObject planeObject;

        public GameObject MainObject;
        public int ARObjPlaceCount = 0;

        //public Vector3 initSize;


        [SerializeField]
        private bool isPlaced;

        // Use this for initialization
        void Start()
        {
            ARObjPlaceCount = 0;
            planeObject = Instantiate(DetectedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
            planeObject.SetActive(false);
            MainObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                PlacedObjectDone();
            }
            if (!isPlaced)
            {
                PlaceObject();
            }
        }


        //Implementation of plane
        void PlaceObject()
        {
            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
                if (!m_IsQuitting && Session.Status.IsValid())
                {
                    planeObject.SetActive(false);
                }

                return;
            }

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(Screen.width / 2, Screen.height / 2, raycastFilter, out hit))
            {
                planeObject.SetActive(true);
                planeObject.GetComponent<Transform>().position = hit.Pose.position;
                planeObject.GetComponent<Transform>().rotation = hit.Pose.rotation;
            }

            Touch touch;
            touch = Input.GetTouch(0);

            //single finger
            if ((Input.touchCount > 0 || touch.phase == TouchPhase.Began))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit rayHit;
                if (Physics.Raycast(ray, out rayHit))
                {
                    if (rayHit.transform.gameObject.layer == 9)
                    {
                        //MainObject.GetComponent<Transform>().rotation = hit.Pose.rotation;
                        PlacedObjectDone();
                    }
                }
            }
            //MainObject.transform.localScale = initSize;
        }


        //onButton call
        public void PlacedObjectDone()
        {
            MainObject.SetActive(true);
            MainObject.GetComponent<Transform>().position = planeObject.GetComponent<Transform>().position;
            ARObjPlaceCount++;
            DataMgr.instance.appDataAnalytics.ObjPlaceCount += ARObjPlaceCount;
            AppManager.instance.uIMGR.ShowMainBtn();
            planeObject.SetActive(false);
            isPlaced = true;
        }

        //surface detection
        public void SearchForSurface()
        {
            planeObject.SetActive(true);
            isPlaced = false;
        }

        public int GetObjPlaceCount()
        {
            return ARObjPlaceCount;
        }
    }
}
