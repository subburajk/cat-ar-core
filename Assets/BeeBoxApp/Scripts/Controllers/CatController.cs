﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace BeeboxCat
{
    public class CatController : MonoBehaviour
    {
        public CatMoveController catMoveController;

        public float angle;
        public Transform bucket;
        public float bucketRotationSpeed;
        public float armRotationSpeed;
        public float minBucketAngle;
        public float maxBucketAngle;

        [SerializeField]
        private float defaultMinBucketAngle;
        [SerializeField]
        private float defaultMaxBucketAngle;

        private int currentResizeIndex;

        [Header("Arm Up Down")]
        [SerializeField]
        public Transform ArmObj;
        [SerializeField]
        private float armMinAngle;
        [SerializeField]
        private float armMaxAngle;
        [SerializeField]
        private float armAngle;


        private float defaultArmAngle;
        private float defaultAngle;
      

        //Input JoyPad 
        public string horizontalAxis = "BucketInput";
        public string verticalAxis = "ArmInput";

        private float inputHorizontal;
        private float inputVertical;
        // Start is called before the first frame update
        void Start()
        {
            defaultAngle = angle;
            defaultArmAngle = armAngle;
            defaultMinBucketAngle = minBucketAngle;
            defaultMaxBucketAngle = maxBucketAngle;
        }

        // Update is called once per frame
        void Update()
        {
            inputHorizontal = SimpleInput.GetAxis(horizontalAxis);
            inputVertical = SimpleInput.GetAxis(verticalAxis);
            ArmUpdate();
            BucketUpdate();
        }


        public void ResetAllPos()
        {
            ArmObj.localEulerAngles = new Vector3(defaultArmAngle, ArmObj.localEulerAngles.y, 0);
            bucket.localEulerAngles = new Vector3(defaultAngle, 0, 0);
            angle = defaultAngle;
            armAngle = defaultArmAngle;
        }

        public void ArmUpdate()
        {
            armAngle += Time.deltaTime * armRotationSpeed * inputVertical;

            armAngle = Mathf.Clamp(armAngle, armMinAngle, armMaxAngle);

            ArmObj.localEulerAngles = new Vector3(armAngle, ArmObj.localEulerAngles.y, 0);
        }

        public void BucketUpdate()
        {

            angle += Time.deltaTime * bucketRotationSpeed * inputHorizontal;

            angle = Mathf.Clamp(angle, minBucketAngle, maxBucketAngle);

            bucket.localEulerAngles = new Vector3(angle, 0, 0);

        }


        public void RescanSurface()
        {
            Scene loadedLevel = SceneManager.GetActiveScene();
            SceneManager.LoadScene(loadedLevel.buildIndex);
        }

        public void ResizingCat(float[] resizeVal, Text resizeTxt)
        {
            var s = this.transform.localScale;
            s.x = resizeVal[currentResizeIndex];
            s.y = resizeVal[currentResizeIndex];
            s.z = resizeVal[currentResizeIndex];
            this.transform.localScale = s;
            currentResizeIndex++;
            if (currentResizeIndex > resizeVal.Length - 1)
            {
                currentResizeIndex = 0;
            }
            resizeTxt.text = "x" + resizeVal[currentResizeIndex];
        }

        public void SetDefaultAngleForBucket()
        {
            minBucketAngle = defaultMinBucketAngle;
            maxBucketAngle = defaultMaxBucketAngle;
        }

        public void SetBucketMinMax(float _min,float _max)
        {
            minBucketAngle = _min;
            maxBucketAngle = _max;
        }
    }
}
