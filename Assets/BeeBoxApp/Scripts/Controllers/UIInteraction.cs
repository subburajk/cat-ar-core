﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class UIInteraction : MonoBehaviour
    {
        public void OnInteracting(bool state)
        {
            AppManager.instance.uIMGR.isInteracting = state;
        }
    }
}
