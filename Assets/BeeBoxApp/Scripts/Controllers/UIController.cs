﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class UIController : MonoBehaviour
    {
        public GameObject mainPanel;
        public GameObject HomeIcon;
        public GameObject CloseIcon;

        public void OnTurbineCtlBtnClicked()
        {

        }


        public void OnExhFanCtlBtnClicked()
        {

        }


        public void OnExhHeatCtlBtnClicked(int state)
        {

        }


        public void OnCloseBtnclicked()
        {

        }


        public void OnOpenBtnclicked()
        {
            mainPanel.SetActive(true);
            HomeIcon.SetActive(false);
            CloseIcon.SetActive(true);
        }


        public void OnTransparancyBtnClicked()
        {

        }
    }
}
