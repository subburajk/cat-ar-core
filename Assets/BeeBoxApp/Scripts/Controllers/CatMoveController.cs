﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleInputNamespace;


namespace BeeboxCat
{
    public class CatMoveController : MonoBehaviour
    {
        [SerializeField]
        private Transform parentTransform;
        [SerializeField]
        private Transform placedTransform;

        [SerializeField]
        private float steeringSpeed;
        [SerializeField]
        private float accelerationSpeed;

        private const int maxAxis = 1;
        private const int minAxis = 0;

        [Header("Rotation Properties")]
        [SerializeField]
        public Transform[] LeftTyres;
        public Transform[] RightTyres;

        [SerializeField]
        private float bodyRotateAngle;
        [SerializeField]
        private float tyreRotateAngle;
        [SerializeField]
        private float tyreMoveRotation;

        [SerializeField]
        private bool isRestrictedArea;
        [SerializeField]
        private float areaMove;


        //Input JoyPad 
        public string horizontalAxis = "Horizontal";
        public string verticalAxis = "Vertical";
        public string jumpButton = "Jump";

        private float inputHorizontal;
        private float inputVertical;


        // Start is called before the first frame update
        void Start()
        {

        }

        /// <summary>
        /// Move Cat And Steering Control Here
        /// </summary>
        // Update is called once per frame
        void FixedUpdate()
        {
            inputHorizontal = SimpleInput.GetAxis(horizontalAxis);
            inputVertical = SimpleInput.GetAxis(verticalAxis);
            MoveUpdate();
            TurnUpdate();
        }

        /// <summary>
        /// For Cat Move
        /// </summary>
        void MoveUpdate()
        {
            float moveSpeed;
            Vector3 updatePos;
            float distance = Vector3.Distance(transform.position, placedTransform.position);
            AppManager.instance.uIBtnMgr.UpdateDistanceTxt(distance);
            moveSpeed = inputVertical * accelerationSpeed * Time.deltaTime;
            updatePos = Vector3.forward * (moveSpeed * parentTransform.localScale.x);
            CatMove(updatePos);
            RotateTyreMesh(moveSpeed * tyreMoveRotation);

        }

        /// <summary>
        /// For Left And Right Normal Rotate
        /// </summary>
        void TurnUpdate()
        {
            float turnAngle = Time.deltaTime * steeringSpeed * inputHorizontal;
            CatTurn(turnAngle);

            if (inputHorizontal < -0.8)
            {
                SelfLeftRotate();
            }
            else if (inputHorizontal > 0.8)
            {
                SelfRightRotate();
            }
        }


        #region Cat Controller
        void CatMove(Vector3 _updatedPos)
        {
            transform.Translate(_updatedPos);

            if (isRestrictedArea)
            {
                Vector3 clampedPosition = transform.position;

                clampedPosition.x = Mathf.Clamp(clampedPosition.x, -areaMove, areaMove);
                clampedPosition.z = Mathf.Clamp(clampedPosition.z, -areaMove, areaMove);

                transform.position = clampedPosition;
            }
        }

        void CatTurn(float _rotateValue)
        {
            transform.Rotate(0, _rotateValue, 0, Space.Self);
        }

        void RotateTyreMesh(float _rotateValue)
        {
            LeftTyres[0].transform.Rotate(_rotateValue, 0, 0, Space.Self);
            LeftTyres[1].transform.Rotate(_rotateValue, 0, 0, Space.Self);
            RightTyres[0].transform.Rotate(_rotateValue, 0, 0, Space.Self);
            RightTyres[1].transform.Rotate(_rotateValue, 0, 0, Space.Self);
        }

        public void SpinRotationEnableDisable(bool _state)
        {
            GetComponent<SpinLogic>().enabled = _state;
        }
        #endregion

        #region For Tyre Rotation

        public void SelfLeftRotate()
        {
            LeftTyres[0].transform.Rotate(-tyreRotateAngle, 0, 0, Space.Self);
            LeftTyres[1].transform.Rotate(-tyreRotateAngle, 0, 0, Space.Self);
            RightTyres[0].transform.Rotate(tyreRotateAngle, 0, 0, Space.Self);
            RightTyres[1].transform.Rotate(tyreRotateAngle, 0, 0, Space.Self);
        }

        public void SelfRightRotate()
        {
            LeftTyres[0].transform.Rotate(tyreRotateAngle, 0, 0, Space.Self);
            LeftTyres[1].transform.Rotate(tyreRotateAngle, 0, 0, Space.Self);
            RightTyres[0].transform.Rotate(-tyreRotateAngle, 0, 0, Space.Self);
            RightTyres[1].transform.Rotate(-tyreRotateAngle, 0, 0, Space.Self);
        }

        #endregion
    }
}
