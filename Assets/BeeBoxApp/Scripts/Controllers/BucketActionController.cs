﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketActionController : MonoBehaviour
{
    public float angle_X;
    public float angle_Y;
    public float angle_Z;
    public Transform bucket;
    public float bucketRotationSpeed;


    private int currentResizeIndex;

    //Input JoyPad 
    public string horizontalAxis = "BucketOpen";
    public string verticalAxis = "BucketActionInput";

    private float inputHorizontal;
    private float inputVertical;

    [Header("Axis Specification")]
    [SerializeField]
    private bool isEnableVerticalInput;
    [SerializeField]
    private bool isEnableHoriInput;


    [Header("Minimum and Maximum Bucket Angle Properties")]
    public float minBucketAngle_X;
    public float maxBucketAngle_X;
    public float minBucketAngle_Y;
    public float maxBucketAngle_Y;
    public float minBucketAngle_Z;
    public float maxBucketAngle_Z;

    [Header("Specific Rotation Angle")]
    [SerializeField]
    private bool isX_Rotate;
    [SerializeField]
    private bool isY_Rotate;
    [SerializeField]
    private bool isZ_Rotate;

    // Start is called before the first frame update
    private void OnEnable()
    {
        angle_X = 0;
        angle_Y = 0;
        angle_Z = 0;
    }


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isEnableHoriInput)
        {
            inputHorizontal = SimpleInput.GetAxis(horizontalAxis);
        }
        if (isEnableVerticalInput)
        { 
            inputVertical = SimpleInput.GetAxis(verticalAxis);
        }
        UpdateBucketController();
    }

    public void UpdateBucketController()
    {
        if (isX_Rotate)
        {
            BucketRotationX();
        }
        if (isY_Rotate)
        {
            BucketRotationY();
        }
        if (isZ_Rotate)
        {
            BucketRotationZ();
        }
    }

    public void BucketRotationX()
    {
        angle_X += Time.deltaTime * bucketRotationSpeed * inputVertical;

        angle_X = Mathf.Clamp(angle_X, minBucketAngle_X, maxBucketAngle_X);

        bucket.localEulerAngles = new Vector3(angle_X, 0, 0);
    }

    public void BucketRotationY()
    {
        angle_Y += Time.deltaTime * bucketRotationSpeed * inputVertical;

        angle_Y = Mathf.Clamp(angle_Y, minBucketAngle_Y, maxBucketAngle_Y);

        bucket.localEulerAngles = new Vector3(0, angle_Y, angle_Z);
    }

    public void BucketRotationZ()
    {
        //Debug.Log(inputHorizontal);
        angle_Z += Time.deltaTime * bucketRotationSpeed * inputHorizontal;

        angle_Z = Mathf.Clamp(angle_Z, minBucketAngle_Z, maxBucketAngle_Z);

        bucket.localEulerAngles = new Vector3(0, angle_Y, angle_Z);
    }
}
