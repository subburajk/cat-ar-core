﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    [CreateAssetMenu]
    public class AccessoriesProperty : ScriptableObject
    {
        public int ID;
        public Sprite buttonIcon;
        public string Name;
    }
}
