﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class AccessoriesCustomization : MonoBehaviour
    {
        public List<GameObject> attachments;
        public int currentIntex;
        public void Start()
        {
      
            OnAttachmentChange(0);
        }
        
        public void OnAttachmentChange(int _index)
        {
            
                attachments[_index].SetActive(true);
            if (currentIntex!=_index) {
                attachments[currentIntex].SetActive(false);
                currentIntex = _index;
            }
        }
    }
}
