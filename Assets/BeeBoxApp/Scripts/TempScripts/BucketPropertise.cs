﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class BucketPropertise : MonoBehaviour
    {
        [SerializeField]
        private float minAngle;
        [SerializeField]
        private float maxAngle;

        //is true set the min and max value from here Otherwise set to default
        [SerializeField]
        private bool isNeedToSetAngles; 

        [SerializeField]
        private List<GameObject> relatedParts;

        // Start is called before the first frame update
        void Start()
        {

        }

        //private void OnInitBucketProperties(List<GameObject> _OtherRelatedParts)
        //{
        //    if (relatedParts != null)
        //    {
        //        foreach (GameObject obj in relatedParts)
        //        {
        //            if (!obj.activeSelf)
        //            {
        //                obj.SetActive(true);
        //            }
        //        }
        //        for (int i=0;i<relatedParts.Count;i++)
        //        {
        //            if (relatedParts)
        //            {

        //            }
        //        }
        //    }
        //    if (isNeedToSetAngles)
        //    {
        //        AppManager.instance.catController.SetBucketMinMax(minAngle, maxAngle);
        //    }
        //    else
        //    {
        //        AppManager.instance.catController.SetDefaultAngleForBucket();
        //    }
        //}

        //private void OnDisableBucketProperties()
        //{
        //    if (relatedParts != null)
        //    {
        //        foreach (GameObject obj in relatedParts)
        //        {
        //            obj.SetActive(false);
        //        }
        //    }
        //}

        private void OnEnable()
        {
            if (relatedParts != null)
            {
                foreach (GameObject obj in relatedParts)
                {
                    if (!obj.activeSelf)
                    {
                        obj.SetActive(true);
                    }
                }
               
            }
            if (isNeedToSetAngles)
            {
                AppManager.instance.catController.SetBucketMinMax(minAngle, maxAngle);
            }
            else
            {
                AppManager.instance.catController.SetDefaultAngleForBucket();
            }
        }

        private void OnDisable()
        {
            if (relatedParts != null)
            {
                foreach (GameObject obj in relatedParts)
                {
                    obj.SetActive(false);
                }
            }
        }
    }
}
