﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BeeboxCat
{
    public class UIMGR : MonoBehaviour
    {
        [Header("Normal Mode Properties")]
        [SerializeField]
        private GameObject RootCatModel;
        [SerializeField]
        private GameObject NormalModeUI;
        [SerializeField]
        private GameObject MainBtn;
        [SerializeField]
        private Image MeasurmentBtnImg;
        private bool isMeasurmentDone = false;
        [SerializeField]
        private Sprite[] measurmentToggle;

        [Header("Mesurment Mode Properties")]
        [SerializeField]
        private GameObject MeasurmentModel;
        [SerializeField]
        private GameObject MeasurmentModeUI;

        [Header("Discord Panel Properties")]
        [SerializeField]
        private GameObject DiscardPanel;
        [SerializeField]
        private GameObject HidePlanePanel;

        [Header("Slider Panel And Text Propertise")]
        [SerializeField]
        private Text HorizontalTxt;
        [SerializeField]
        private Text VerticalTxt;
        [SerializeField]
        private Slider HorizontalSlider;
        [SerializeField]
        private Slider VerticalSlider;
        [SerializeField]
        private Text HorizontalSliderTxt;
        [SerializeField]
        private Text VerticalSliderTxt;

        [Header("Plane Properties")]
        [SerializeField]
        private Transform HorizontalPlane;
        [SerializeField]
        private Transform VerticalPlane;
        [SerializeField]
        private TextMesh HorizontalMeterTxt;
        [SerializeField]
        private TextMesh VerticalMeterTxt;

        public bool isInteracting;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnHorizontalValueChange()
        {
            var pos = HorizontalPlane.localPosition;
            pos.y = HorizontalSlider.value;
            HorizontalPlane.localPosition = pos;
            HorizontalMeterTxt.text = pos.y.ToString("0.00") + " m";
            HorizontalSliderTxt.text = "" + pos.y.ToString("0.00") + " m";
        }

        public void OnVerticalValueChange()
        {
            var pos = VerticalPlane.localPosition;
            pos.x = VerticalSlider.value;
            VerticalPlane.localPosition = pos;
            VerticalSliderTxt.text = "" + pos.x.ToString("0.00") + " m";
            VerticalMeterTxt.text = pos.x.ToString("0.00") + " m";
        }

        public void HorizontalPlusMinus(float _val)
        {
            HorizontalSlider.value += _val;
        }

        public void VerticalPlusMinus(float _val)
        {
            VerticalSlider.value += _val;
        }

        public void ShowMainBtn()
        {
            MainBtn.SetActive(true);
        }

        public void ShowAllUI()
        {
            MainBtn.SetActive(true);
            AppManager.instance.uIBtnMgr.ShowAllPanel();
        }

        public void HideAllUI()
        {
            MainBtn.SetActive(false);
            AppManager.instance.uIBtnMgr.HideAllPanel();
        }

        public void OnModeChange(bool NormalMode)
        {
            if (isMeasurmentDone)
            {
                On_HidePanel_Show();
            }
            else
            {
                AppManager.instance.uIBtnMgr.CheckAndHideAccessoriesPanel();

                AppManager.instance.catController.ResetAllPos();
                MeasurmentModel.transform.position = RootCatModel.transform.position;
                RootCatModel.GetComponent<SpinLogic>().enabled = NormalMode;
                MeasurmentModel.GetComponent<SpinLogic>().enabled = !NormalMode;
                MainBtn.SetActive(NormalMode);
                MeasurmentModel.SetActive(!NormalMode);
                NormalModeUI.SetActive(NormalMode);
                MeasurmentModeUI.SetActive(!NormalMode);
            }

        }

        IEnumerator OnModeConform(bool val)
        {
            OnModeChange(true);
            MeasurmentModel.SetActive(val);
            yield return null;
        }

        //Discard the Changes
        public void OnBackBtn()
        {
            DiscardPanel.SetActive(true);
        }

        //Apply the Changes
        public void OnOKBtn()
        {
            StartCoroutine(OnModeConform(true));
            DiscardPanel.SetActive(false);
            isMeasurmentDone = true;
            ChangeMeasurmentIcon(1);
        }

        //Apply the Changes
        public void OnDiscardOKBtn()
        {
            HorizontalSlider.value = 3.71f;
            VerticalSlider.value = 2.7f;
            StartCoroutine(OnModeConform(false));
            DiscardPanel.SetActive(false);
        }

        public void OnCancelBtn()
        {
            DiscardPanel.SetActive(false);
        }

        public void On_HidePanel_Show()
        {
            HidePlanePanel.SetActive(true);
        }

        public void OkBtnHidePanel()
        {
            MeasurmentModel.SetActive(false);
            isMeasurmentDone = false;
            ChangeMeasurmentIcon(0);
            HidePlanePanel.SetActive(false);
        }

        public void CancelBtnHidePane()
        {
            HidePlanePanel.SetActive(false);
        }

        public void ChangeMeasurmentIcon(int _val)
        {
            MeasurmentBtnImg.sprite = measurmentToggle[_val];
        }
    }
}
