﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class AudioMGR : MonoBehaviour
    {
        public AudioSource audioSrc;
        public AudioSource audioSrcBreakGalss;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void RollingAudioPlay()
        {
            audioSrc.Play();
        }

        public void RollingAudioStop()
        {
            audioSrc.Stop();
        }

        public void RollingAudioPause()
        {
            audioSrc.Pause();
        }

        public void OnPlayCrashSound()
        {
            if (audioSrcBreakGalss.isPlaying)
            {
                audioSrcBreakGalss.Stop();
            }
            audioSrcBreakGalss.Play();
        }
    }
}
