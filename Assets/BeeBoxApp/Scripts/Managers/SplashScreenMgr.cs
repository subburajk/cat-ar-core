﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BeeboxCat
{
    public class SplashScreenMgr : MonoBehaviour
    {
        [SerializeField]
        private float waitingTime;
        public GameObject loginPanel;

        private void Start()
        {
            StartCoroutine(WaitTime());
        }


        IEnumerator WaitTime()
        {
            yield return new WaitForSeconds(waitingTime);
            this.gameObject.SetActive(false);
        }
    }
}
