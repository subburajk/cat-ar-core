﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class AppManager : MonoBehaviour
    {
        public static AppManager instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        //All Class Reference Here
        public ARController arController;
        public CatController catController;
        public AudioMGR audioMGR;
        public UIMGR uIMGR;
        public UIBtnMgr uIBtnMgr;
        public AccessoriesCustomization accessoriesCustomization;
    }
}
