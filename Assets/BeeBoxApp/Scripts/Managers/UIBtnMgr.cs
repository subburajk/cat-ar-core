﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BeeboxCat
{
    public class UIBtnMgr : MonoBehaviour
    {
        [Header("Distance Text")]
        public Text distanceTxt;

        private bool isMainPanelHide = false;
        public GameObject MainPanel;
        public GameObject MeasureMentObject;
        public GameObject CommonBtns;
        public GameObject BtnToShowForAccessories;
        public GameObject BtnToShowForMovement;
        public GameObject JoysticPanel;
        public GameObject AccessoriesPanel;
        public bool isAccessoriesOn;
        public bool isMovementOn;

        [SerializeField]
        private Sprite[] changeableIcon;

        [SerializeField]
        private Image accessoriesImg;
        [SerializeField]
        private Image movementImg;

        [SerializeField]
        private float[] resizeVal;
        [SerializeField]
        private Text resizeTxt;

        //Update Distance Text
        public void UpdateDistanceTxt(float _distance)
        {
            distanceTxt.text = "Distance :" + _distance.ToString("0.00") + "m";
        }

        public void ShowHideAllPanels()
        {
            isMainPanelHide = !isMainPanelHide;
            MainPanel.SetActive(isMainPanelHide);
            MeasureMentObject.SetActive(isMainPanelHide);
            CheckAndHideAccessoriesPanel();
        }

        public void HideAllPanel()
        {
            MainPanel.SetActive(false);
        }
        public void ShowAllPanel()
        {
            MainPanel.SetActive(true);
        }

        public void OnResizeBtnClicked()
        {
            AppManager.instance.catController.ResizingCat(resizeVal, resizeTxt);
        }

        public void OnAccessoriesParentBtnClicked()
        {
            //isAccessoriesOn = !isAccessoriesOn;
            //if (isAccessoriesOn)
            //{
            //    ShowOrHideCommonBtns(CommonBtns, false);
            //    ShowOrHideCommonBtns(BtnToShowForAccessories.transform.parent.gameObject, true);
            //    accessoriesImg.sprite = changeableIcon[0];
            //}
            //else
            //{
            //    ShowOrHideCommonBtns(BtnToShowForAccessories.transform.parent.gameObject, false);
            //    ShowOrHideCommonBtns(CommonBtns, true);
            //    accessoriesImg.sprite = changeableIcon[1];
            //}
            //BtnToShowForAccessories.SetActive(true);
            //BtnToShowForAccessories.transform.parent.gameObject.SetActive(true);
            if (AccessoriesPanel.activeSelf)
            {
                AccessoriesPanel.SetActive(false);
            }
            else
            {
                AccessoriesPanel.SetActive(true);
            }
        }

        public void CheckAndHideAccessoriesPanel()
        {
            if (AccessoriesPanel.activeSelf)
            {
                AccessoriesPanel.SetActive(false);
            }
        }

        public void OnMovementParentBtnClicked()
        {
            CheckAndHideAccessoriesPanel(); //AccessoriesPanel Check
            
            isMovementOn = !isMovementOn;
            if (isMovementOn)
            {
                ShowOrHideCommonBtns(CommonBtns, false);
                Debug.Log(BtnToShowForMovement.transform.parent.gameObject.name);
                ShowOrHideCommonBtns(BtnToShowForMovement.transform.parent.gameObject, true);
                JoysticPanel.SetActive(true);
                movementImg.sprite = changeableIcon[0];
                //Disable Spin Rotation
                AppManager.instance.catController.catMoveController.SpinRotationEnableDisable(false);
            }
            else
            {
                ShowOrHideCommonBtns(CommonBtns, true);
                ShowOrHideCommonBtns(BtnToShowForMovement.transform.parent.gameObject, false);
                JoysticPanel.SetActive(false);
                movementImg.sprite = changeableIcon[2];
                //enable Spin Rotation
                AppManager.instance.catController.catMoveController.SpinRotationEnableDisable(true);
            }

            BtnToShowForMovement.SetActive(true);
            BtnToShowForMovement.transform.parent.gameObject.SetActive(true);
        }

        public void ShowOrHideCommonBtns(GameObject parent, bool val)
        {
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                parent.transform.GetChild(i).gameObject.SetActive(val);
            }
        }
    }
}
