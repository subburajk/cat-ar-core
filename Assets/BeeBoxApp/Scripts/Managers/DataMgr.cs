﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeeboxCat
{
    public class DataMgr : MonoBehaviour
    {
        public static DataMgr instance = null;
        //All Class Reference Here
        public JsonReadWrite jsonReadWrite;
        public LocalDataMgr localDataMgr;
        public AppDataAnalytics appDataAnalytics;
        public ServerDataManager serverDataMgr;
        [Header("Need To Assign")]
        public SceneMgr sceneMgr;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
            DontDestroyOnLoad(this);
            jsonReadWrite = transform.GetComponent<JsonReadWrite>();
            localDataMgr = transform.GetComponent<LocalDataMgr>();
            appDataAnalytics = transform.GetComponent<AppDataAnalytics>();
            serverDataMgr = transform.GetComponent<ServerDataManager>();
        }


    }
}
