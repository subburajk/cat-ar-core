﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BeeboxCat
{
    public class SceneMgr : MonoBehaviour
    {
        public bool isAcivateScene = false;
        void Start()
        {
            StartCoroutine(LoadScene());
        }

        public void AcitivateScene()
        {
            isAcivateScene = true;

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                isAcivateScene = true;
            }
        }
        IEnumerator LoadScene()
        {
            yield return null;

            //Begin to load the Scene you specify
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Cat");
            //Don't let the Scene activate until you allow it to
            asyncOperation.allowSceneActivation = false;
            //        Debug.Log("Pro :" + asyncOperation.progress);
            //When the load is still in progress, output the Text and progress bar
            while (!asyncOperation.isDone)
            {
                //Output the current progress
                // Debug.Log("Loading progress: " + (asyncOperation.progress * 100) + "%");

                // Check if the load has finished
                if (asyncOperation.progress >= 0.9f)
                {
                    //Wait to you press the space key to activate the Scene
                    if (isAcivateScene)
                    {
                        //Activate the Scene
                        //  Debug.Log("Press the space bar to continue");
                        asyncOperation.allowSceneActivation = true;
                        isAcivateScene = false;
                    }
                }

                yield return null;
            }
        }
    }
}

