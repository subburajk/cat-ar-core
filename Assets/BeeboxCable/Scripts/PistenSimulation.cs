﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MGS.CurveHose;

namespace BeeboxCable
{
    public class PistenSimulation : MonoBehaviour
    {
        public HermiteHose hemiteHoseCable;
        public Transform startPos;
        public Transform endPos;

        // Start is called before the first frame update
        public void SimulatePisten()
        {
            hemiteHoseCable.SetAnchorAt(0, startPos.position);
            hemiteHoseCable.SetAnchorAt(hemiteHoseCable.AnchorsCount - 1, endPos.position);

            hemiteHoseCable.Rebuild();
        }

        private void FixedUpdate()
        {
            SimulatePisten();
        }
    }
}
