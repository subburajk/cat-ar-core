﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MGS.CurveHose;

namespace BeeboxCable
{
    public class CableSimulation : MonoBehaviour
    {
        public HermiteHose hemiteHoseCable;
        public BezierHose bezierHoseCable;
        public Transform startPos;
        public Transform endPos;
        public Transform endTangentPoint;
        public Transform startTangentPoint;
        public bool isBezierCable;
        // Start is called before the first frame update
        void Start()
        {

        }

        public void FixedUpdate()
        {
            if (isBezierCable)
            {
                SetBezierCable();
            }
            else
            {

                SetPos();
            }
        }

        public void SetPos()
        {
            hemiteHoseCable.SetAnchorAt(0, startPos.position);
            hemiteHoseCable.SetAnchorAt(hemiteHoseCable.AnchorsCount-1, endPos.position);
            
            hemiteHoseCable.Rebuild();
        }

        public void SetBezierCable()
        {
            bezierHoseCable.StartPoint = startPos.position;
            bezierHoseCable.EndPoint = endPos.position;
            bezierHoseCable.EndTangentPoint = endTangentPoint.position;
            bezierHoseCable.StartTangentPoint = startTangentPoint.position;
            bezierHoseCable.Rebuild();
        }
    }
}
